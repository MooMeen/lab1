<?xml version="1.0" encoding="UTF-8" ?>

<Form version="1.6" maxVersion="1.9" type="org.netbeans.modules.form.forminfo.JFrameFormInfo">
  <NonVisualComponents>
    <Component class="javax.swing.ButtonGroup" name="buttonGroup1">
    </Component>
  </NonVisualComponents>
  <Properties>
    <Property name="defaultCloseOperation" type="int" value="3"/>
  </Properties>
  <SyntheticProperties>
    <SyntheticProperty name="formSizePolicy" type="int" value="1"/>
    <SyntheticProperty name="generateCenter" type="boolean" value="false"/>
  </SyntheticProperties>
  <AuxValues>
    <AuxValue name="FormSettings_autoResourcing" type="java.lang.Integer" value="0"/>
    <AuxValue name="FormSettings_autoSetComponentName" type="java.lang.Boolean" value="false"/>
    <AuxValue name="FormSettings_generateFQN" type="java.lang.Boolean" value="true"/>
    <AuxValue name="FormSettings_generateMnemonicsCode" type="java.lang.Boolean" value="false"/>
    <AuxValue name="FormSettings_i18nAutoMode" type="java.lang.Boolean" value="false"/>
    <AuxValue name="FormSettings_layoutCodeTarget" type="java.lang.Integer" value="1"/>
    <AuxValue name="FormSettings_listenerGenerationStyle" type="java.lang.Integer" value="0"/>
    <AuxValue name="FormSettings_variablesLocal" type="java.lang.Boolean" value="false"/>
    <AuxValue name="FormSettings_variablesModifier" type="java.lang.Integer" value="2"/>
  </AuxValues>

  <Layout>
    <DimensionLayout dim="0">
      <Group type="103" groupAlignment="0" attributes="0">
          <Group type="102" alignment="0" attributes="0">
              <EmptySpace min="-2" pref="365" max="-2" attributes="0"/>
              <Component id="jPanel3" min="-2" max="-2" attributes="0"/>
              <EmptySpace max="32767" attributes="0"/>
          </Group>
      </Group>
    </DimensionLayout>
    <DimensionLayout dim="1">
      <Group type="103" groupAlignment="0" attributes="0">
          <Group type="102" alignment="0" attributes="0">
              <EmptySpace min="-2" pref="215" max="-2" attributes="0"/>
              <Component id="jPanel3" min="-2" max="-2" attributes="0"/>
              <EmptySpace max="32767" attributes="0"/>
          </Group>
      </Group>
    </DimensionLayout>
  </Layout>
  <SubComponents>
    <Container class="javax.swing.JPanel" name="jPanel3">

      <Layout class="org.netbeans.modules.form.compat2.layouts.DesignCardLayout"/>
      <SubComponents>
        <Container class="javax.swing.JPanel" name="jPanel1">
          <Properties>
            <Property name="background" type="java.awt.Color" editor="org.netbeans.beaninfo.editors.ColorEditor">
              <Color blue="99" green="99" red="0" type="rgb"/>
            </Property>
          </Properties>
          <Constraints>
            <Constraint layoutClass="org.netbeans.modules.form.compat2.layouts.DesignCardLayout" value="org.netbeans.modules.form.compat2.layouts.DesignCardLayout$CardConstraintsDescription">
              <CardConstraints cardName="main page"/>
            </Constraint>
          </Constraints>

          <Layout>
            <DimensionLayout dim="0">
              <Group type="103" groupAlignment="0" attributes="0">
                  <Group type="102" alignment="0" attributes="0">
                      <EmptySpace max="-2" attributes="0"/>
                      <Group type="103" groupAlignment="0" attributes="0">
                          <Group type="102" alignment="0" attributes="0">
                              <Group type="103" groupAlignment="0" max="-2" attributes="0">
                                  <Component id="jLabel1" alignment="0" min="-2" max="-2" attributes="0"/>
                                  <Component id="jLabel2" alignment="0" min="-2" max="-2" attributes="0"/>
                                  <Group type="102" alignment="0" attributes="0">
                                      <Group type="103" groupAlignment="0" attributes="0">
                                          <Component id="jLabel3" min="-2" max="-2" attributes="0"/>
                                          <Component id="jComboBox1" min="-2" pref="45" max="-2" attributes="0"/>
                                      </Group>
                                      <EmptySpace min="-2" pref="38" max="-2" attributes="0"/>
                                      <Group type="103" groupAlignment="0" attributes="0">
                                          <Component id="jLabel4" alignment="0" min="-2" max="-2" attributes="0"/>
                                          <Component id="jComboBox2" alignment="0" min="-2" pref="47" max="-2" attributes="0"/>
                                      </Group>
                                      <EmptySpace pref="51" max="32767" attributes="0"/>
                                      <Group type="103" groupAlignment="0" attributes="0">
                                          <Component id="jLabel5" min="-2" max="-2" attributes="0"/>
                                          <Component id="jComboBox3" min="-2" pref="47" max="-2" attributes="0"/>
                                      </Group>
                                  </Group>
                                  <Component id="jTextField1" max="32767" attributes="0"/>
                                  <Component id="jXDatePicker1" max="32767" attributes="0"/>
                              </Group>
                              <EmptySpace min="-2" pref="46" max="-2" attributes="0"/>
                              <Group type="103" groupAlignment="0" attributes="0">
                                  <Group type="102" attributes="0">
                                      <Group type="103" groupAlignment="0" attributes="0">
                                          <Component id="jLabel6" min="-2" max="-2" attributes="0"/>
                                          <Component id="jTextField3" min="-2" pref="280" max="-2" attributes="0"/>
                                          <Component id="jLabel7" alignment="0" min="-2" max="-2" attributes="0"/>
                                          <Group type="102" alignment="1" attributes="0">
                                              <EmptySpace min="-2" pref="1" max="-2" attributes="0"/>
                                              <Component id="jXDatePicker2" min="-2" pref="279" max="-2" attributes="0"/>
                                          </Group>
                                      </Group>
                                      <EmptySpace type="unrelated" max="32767" attributes="0"/>
                                      <Group type="103" groupAlignment="0" attributes="0">
                                          <Group type="102" alignment="1" attributes="0">
                                              <Group type="103" groupAlignment="0" attributes="0">
                                                  <Component id="jLabel9" min="-2" max="-2" attributes="0"/>
                                                  <Component id="jTextField5" min="-2" pref="171" max="-2" attributes="0"/>
                                              </Group>
                                              <EmptySpace min="-2" pref="161" max="-2" attributes="0"/>
                                          </Group>
                                          <Group type="102" alignment="1" attributes="0">
                                              <Group type="103" groupAlignment="0" attributes="0">
                                                  <Component id="jRadioButton2" min="-2" max="-2" attributes="0"/>
                                                  <Component id="jRadioButton1" min="-2" max="-2" attributes="0"/>
                                              </Group>
                                              <EmptySpace min="-2" pref="217" max="-2" attributes="0"/>
                                          </Group>
                                      </Group>
                                  </Group>
                                  <Group type="102" attributes="0">
                                      <Group type="103" groupAlignment="0" attributes="0">
                                          <Component id="jLabel8" alignment="0" min="-2" max="-2" attributes="0"/>
                                          <Component id="jComboBox4" min="-2" pref="105" max="-2" attributes="0"/>
                                      </Group>
                                      <EmptySpace max="32767" attributes="0"/>
                                  </Group>
                              </Group>
                          </Group>
                          <Group type="102" alignment="0" attributes="0">
                              <Component id="jLabel10" min="-2" max="-2" attributes="0"/>
                              <EmptySpace max="32767" attributes="0"/>
                          </Group>
                          <Group type="102" alignment="0" attributes="0">
                              <Component id="jButton1" min="-2" pref="140" max="-2" attributes="0"/>
                              <EmptySpace min="0" pref="0" max="32767" attributes="0"/>
                          </Group>
                      </Group>
                  </Group>
              </Group>
            </DimensionLayout>
            <DimensionLayout dim="1">
              <Group type="103" groupAlignment="0" attributes="0">
                  <Group type="102" alignment="0" attributes="0">
                      <EmptySpace min="-2" pref="19" max="-2" attributes="0"/>
                      <Component id="jLabel10" min="-2" max="-2" attributes="0"/>
                      <EmptySpace min="-2" pref="43" max="-2" attributes="0"/>
                      <Group type="103" groupAlignment="3" attributes="0">
                          <Component id="jLabel1" alignment="3" min="-2" pref="27" max="-2" attributes="0"/>
                          <Component id="jLabel6" alignment="3" min="-2" max="-2" attributes="0"/>
                          <Component id="jRadioButton1" alignment="3" min="-2" max="-2" attributes="0"/>
                      </Group>
                      <EmptySpace max="-2" attributes="0"/>
                      <Group type="103" groupAlignment="0" max="-2" attributes="0">
                          <Group type="103" groupAlignment="3" attributes="0">
                              <Component id="jTextField3" alignment="3" max="32767" attributes="0"/>
                              <Component id="jRadioButton2" alignment="3" min="-2" max="-2" attributes="0"/>
                          </Group>
                          <Component id="jTextField1" max="32767" attributes="0"/>
                      </Group>
                      <EmptySpace type="separate" max="-2" attributes="0"/>
                      <Group type="103" groupAlignment="0" attributes="0">
                          <Component id="jLabel2" min="-2" max="-2" attributes="0"/>
                          <Group type="103" groupAlignment="3" attributes="0">
                              <Component id="jLabel7" alignment="3" min="-2" pref="23" max="-2" attributes="0"/>
                              <Component id="jLabel9" alignment="3" min="-2" max="-2" attributes="0"/>
                          </Group>
                      </Group>
                      <EmptySpace type="unrelated" max="-2" attributes="0"/>
                      <Group type="103" groupAlignment="0" max="-2" attributes="0">
                          <Component id="jXDatePicker2" pref="34" max="32767" attributes="0"/>
                          <Component id="jXDatePicker1" max="32767" attributes="0"/>
                          <Component id="jTextField5" alignment="0" max="32767" attributes="0"/>
                      </Group>
                      <EmptySpace type="separate" max="-2" attributes="0"/>
                      <Group type="103" groupAlignment="3" attributes="0">
                          <Component id="jLabel3" alignment="3" min="-2" max="-2" attributes="0"/>
                          <Component id="jLabel4" alignment="3" min="-2" max="-2" attributes="0"/>
                          <Component id="jLabel5" alignment="3" min="-2" max="-2" attributes="0"/>
                          <Component id="jLabel8" alignment="3" min="-2" max="-2" attributes="0"/>
                      </Group>
                      <EmptySpace max="-2" attributes="0"/>
                      <Group type="103" groupAlignment="0" max="-2" attributes="0">
                          <Component id="jComboBox3" min="-2" pref="36" max="-2" attributes="0"/>
                          <Component id="jComboBox2" max="32767" attributes="0"/>
                          <Component id="jComboBox1" max="32767" attributes="0"/>
                          <Component id="jComboBox4" alignment="1" max="32767" attributes="0"/>
                      </Group>
                      <EmptySpace pref="32" max="32767" attributes="0"/>
                      <Component id="jButton1" min="-2" pref="53" max="-2" attributes="0"/>
                      <EmptySpace max="-2" attributes="0"/>
                  </Group>
              </Group>
            </DimensionLayout>
          </Layout>
          <SubComponents>
            <Component class="javax.swing.JLabel" name="jLabel1">
              <Properties>
                <Property name="font" type="java.awt.Font" editor="org.netbeans.beaninfo.editors.FontEditor">
                  <Font name="Tahoma" size="20" style="0"/>
                </Property>
                <Property name="foreground" type="java.awt.Color" editor="org.netbeans.beaninfo.editors.ColorEditor">
                  <Color blue="ec" green="ee" red="ee" type="rgb"/>
                </Property>
                <Property name="text" type="java.lang.String" value="Flying from"/>
              </Properties>
            </Component>
            <Component class="javax.swing.JTextField" name="jTextField1">
              <Properties>
                <Property name="font" type="java.awt.Font" editor="org.netbeans.beaninfo.editors.FontEditor">
                  <Font name="Tahoma" size="14" style="0"/>
                </Property>
                <Property name="text" type="java.lang.String" value="Type a city or airport"/>
              </Properties>
            </Component>
            <Component class="javax.swing.JLabel" name="jLabel2">
              <Properties>
                <Property name="font" type="java.awt.Font" editor="org.netbeans.beaninfo.editors.FontEditor">
                  <Font name="Tahoma" size="20" style="0"/>
                </Property>
                <Property name="foreground" type="java.awt.Color" editor="org.netbeans.beaninfo.editors.ColorEditor">
                  <Color blue="ec" green="ee" red="ee" type="rgb"/>
                </Property>
                <Property name="text" type="java.lang.String" value="Departing"/>
              </Properties>
            </Component>
            <Component class="javax.swing.JLabel" name="jLabel3">
              <Properties>
                <Property name="font" type="java.awt.Font" editor="org.netbeans.beaninfo.editors.FontEditor">
                  <Font name="Tahoma" size="20" style="0"/>
                </Property>
                <Property name="foreground" type="java.awt.Color" editor="org.netbeans.beaninfo.editors.ColorEditor">
                  <Color blue="ec" green="ee" red="ee" type="rgb"/>
                </Property>
                <Property name="text" type="java.lang.String" value="Adults"/>
              </Properties>
            </Component>
            <Component class="javax.swing.JLabel" name="jLabel4">
              <Properties>
                <Property name="font" type="java.awt.Font" editor="org.netbeans.beaninfo.editors.FontEditor">
                  <Font name="Tahoma" size="20" style="0"/>
                </Property>
                <Property name="foreground" type="java.awt.Color" editor="org.netbeans.beaninfo.editors.ColorEditor">
                  <Color blue="ec" green="ee" red="ee" type="rgb"/>
                </Property>
                <Property name="text" type="java.lang.String" value="Children"/>
              </Properties>
            </Component>
            <Component class="javax.swing.JLabel" name="jLabel5">
              <Properties>
                <Property name="font" type="java.awt.Font" editor="org.netbeans.beaninfo.editors.FontEditor">
                  <Font name="Tahoma" size="20" style="0"/>
                </Property>
                <Property name="foreground" type="java.awt.Color" editor="org.netbeans.beaninfo.editors.ColorEditor">
                  <Color blue="ec" green="ee" red="ee" type="rgb"/>
                </Property>
                <Property name="text" type="java.lang.String" value="Infants"/>
              </Properties>
            </Component>
            <Component class="javax.swing.JComboBox" name="jComboBox1">
              <Properties>
                <Property name="editable" type="boolean" value="true"/>
                <Property name="model" type="javax.swing.ComboBoxModel" editor="org.netbeans.modules.form.editors2.ComboBoxModelEditor">
                  <StringArray count="6">
                    <StringItem index="0" value="1"/>
                    <StringItem index="1" value="2"/>
                    <StringItem index="2" value="3"/>
                    <StringItem index="3" value="4"/>
                    <StringItem index="4" value="5"/>
                    <StringItem index="5" value="6"/>
                  </StringArray>
                </Property>
                <Property name="name" type="java.lang.String" value="" noResource="true"/>
              </Properties>
              <Events>
                <EventHandler event="actionPerformed" listener="java.awt.event.ActionListener" parameters="java.awt.event.ActionEvent" handler="jComboBox1ActionPerformed"/>
              </Events>
            </Component>
            <Component class="javax.swing.JComboBox" name="jComboBox2">
              <Properties>
                <Property name="editable" type="boolean" value="true"/>
                <Property name="model" type="javax.swing.ComboBoxModel" editor="org.netbeans.modules.form.editors2.ComboBoxModelEditor">
                  <StringArray count="7">
                    <StringItem index="0" value="0"/>
                    <StringItem index="1" value="1"/>
                    <StringItem index="2" value="2"/>
                    <StringItem index="3" value="3"/>
                    <StringItem index="4" value="4"/>
                    <StringItem index="5" value="5"/>
                    <StringItem index="6" value="6"/>
                  </StringArray>
                </Property>
              </Properties>
            </Component>
            <Component class="javax.swing.JComboBox" name="jComboBox3">
              <Properties>
                <Property name="model" type="javax.swing.ComboBoxModel" editor="org.netbeans.modules.form.editors2.ComboBoxModelEditor">
                  <StringArray count="7">
                    <StringItem index="0" value="0"/>
                    <StringItem index="1" value="1"/>
                    <StringItem index="2" value="2"/>
                    <StringItem index="3" value="3"/>
                    <StringItem index="4" value="4"/>
                    <StringItem index="5" value="5"/>
                    <StringItem index="6" value="6"/>
                  </StringArray>
                </Property>
              </Properties>
            </Component>
            <Component class="javax.swing.JLabel" name="jLabel6">
              <Properties>
                <Property name="font" type="java.awt.Font" editor="org.netbeans.beaninfo.editors.FontEditor">
                  <Font name="Tahoma" size="20" style="0"/>
                </Property>
                <Property name="foreground" type="java.awt.Color" editor="org.netbeans.beaninfo.editors.ColorEditor">
                  <Color blue="ec" green="ee" red="ee" type="rgb"/>
                </Property>
                <Property name="text" type="java.lang.String" value="Flying to"/>
              </Properties>
            </Component>
            <Component class="javax.swing.JTextField" name="jTextField3">
              <Properties>
                <Property name="font" type="java.awt.Font" editor="org.netbeans.beaninfo.editors.FontEditor">
                  <Font name="Tahoma" size="14" style="0"/>
                </Property>
                <Property name="text" type="java.lang.String" value="Type a city or airport"/>
              </Properties>
            </Component>
            <Component class="javax.swing.JLabel" name="jLabel7">
              <Properties>
                <Property name="font" type="java.awt.Font" editor="org.netbeans.beaninfo.editors.FontEditor">
                  <Font name="Tahoma" size="20" style="0"/>
                </Property>
                <Property name="foreground" type="java.awt.Color" editor="org.netbeans.beaninfo.editors.ColorEditor">
                  <Color blue="ec" green="ee" red="ee" type="rgb"/>
                </Property>
                <Property name="text" type="java.lang.String" value="Returning"/>
              </Properties>
            </Component>
            <Component class="javax.swing.JLabel" name="jLabel8">
              <Properties>
                <Property name="font" type="java.awt.Font" editor="org.netbeans.beaninfo.editors.FontEditor">
                  <Font name="Tahoma" size="20" style="0"/>
                </Property>
                <Property name="foreground" type="java.awt.Color" editor="org.netbeans.beaninfo.editors.ColorEditor">
                  <Color blue="ec" green="ee" red="ee" type="rgb"/>
                </Property>
                <Property name="text" type="java.lang.String" value="Cabin"/>
              </Properties>
            </Component>
            <Component class="javax.swing.JComboBox" name="jComboBox4">
              <Properties>
                <Property name="font" type="java.awt.Font" editor="org.netbeans.beaninfo.editors.FontEditor">
                  <Font name="Tahoma" size="14" style="0"/>
                </Property>
                <Property name="model" type="javax.swing.ComboBoxModel" editor="org.netbeans.modules.form.editors2.ComboBoxModelEditor">
                  <StringArray count="3">
                    <StringItem index="0" value="economy"/>
                    <StringItem index="1" value="bussiness"/>
                    <StringItem index="2" value="first"/>
                  </StringArray>
                </Property>
              </Properties>
            </Component>
            <Component class="javax.swing.JLabel" name="jLabel9">
              <Properties>
                <Property name="font" type="java.awt.Font" editor="org.netbeans.beaninfo.editors.FontEditor">
                  <Font name="Tahoma" size="20" style="0"/>
                </Property>
                <Property name="foreground" type="java.awt.Color" editor="org.netbeans.beaninfo.editors.ColorEditor">
                  <Color blue="ec" green="ee" red="ee" type="rgb"/>
                </Property>
                <Property name="text" type="java.lang.String" value="or How long"/>
              </Properties>
            </Component>
            <Component class="javax.swing.JTextField" name="jTextField5">
            </Component>
            <Component class="javax.swing.JRadioButton" name="jRadioButton1">
              <Properties>
                <Property name="buttonGroup" type="javax.swing.ButtonGroup" editor="org.netbeans.modules.form.RADComponent$ButtonGroupPropertyEditor">
                  <ComponentRef name="buttonGroup1"/>
                </Property>
                <Property name="font" type="java.awt.Font" editor="org.netbeans.beaninfo.editors.FontEditor">
                  <Font name="Tahoma" size="14" style="0"/>
                </Property>
                <Property name="foreground" type="java.awt.Color" editor="org.netbeans.beaninfo.editors.ColorEditor">
                  <Color blue="ec" green="ee" red="ee" type="rgb"/>
                </Property>
                <Property name="text" type="java.lang.String" value=" One way"/>
              </Properties>
              <Events>
                <EventHandler event="actionPerformed" listener="java.awt.event.ActionListener" parameters="java.awt.event.ActionEvent" handler="jRadioButton1ActionPerformed"/>
              </Events>
            </Component>
            <Component class="javax.swing.JRadioButton" name="jRadioButton2">
              <Properties>
                <Property name="buttonGroup" type="javax.swing.ButtonGroup" editor="org.netbeans.modules.form.RADComponent$ButtonGroupPropertyEditor">
                  <ComponentRef name="buttonGroup1"/>
                </Property>
                <Property name="font" type="java.awt.Font" editor="org.netbeans.beaninfo.editors.FontEditor">
                  <Font name="Tahoma" size="14" style="0"/>
                </Property>
                <Property name="foreground" type="java.awt.Color" editor="org.netbeans.beaninfo.editors.ColorEditor">
                  <Color blue="ec" green="ee" red="ee" type="rgb"/>
                </Property>
                <Property name="text" type="java.lang.String" value="Return"/>
              </Properties>
            </Component>
            <Component class="javax.swing.JLabel" name="jLabel10">
              <Properties>
                <Property name="font" type="java.awt.Font" editor="org.netbeans.beaninfo.editors.FontEditor">
                  <Font name="Tahoma" size="36" style="0"/>
                </Property>
                <Property name="foreground" type="java.awt.Color" editor="org.netbeans.beaninfo.editors.ColorEditor">
                  <Color blue="ec" green="ee" red="ee" type="rgb"/>
                </Property>
                <Property name="text" type="java.lang.String" value="FLIGHTS"/>
              </Properties>
            </Component>
            <Component class="javax.swing.JButton" name="jButton1">
              <Properties>
                <Property name="background" type="java.awt.Color" editor="org.netbeans.beaninfo.editors.ColorEditor">
                  <Color blue="66" green="0" red="cc" type="rgb"/>
                </Property>
                <Property name="font" type="java.awt.Font" editor="org.netbeans.beaninfo.editors.FontEditor">
                  <Font name="Tahoma" size="36" style="0"/>
                </Property>
                <Property name="foreground" type="java.awt.Color" editor="org.netbeans.beaninfo.editors.ColorEditor">
                  <Color blue="ec" green="ee" red="ee" type="rgb"/>
                </Property>
                <Property name="text" type="java.lang.String" value="Search"/>
              </Properties>
              <Events>
                <EventHandler event="actionPerformed" listener="java.awt.event.ActionListener" parameters="java.awt.event.ActionEvent" handler="jButton1ActionPerformed"/>
              </Events>
            </Component>
            <Component class="org.jdesktop.swingx.JXDatePicker" name="jXDatePicker1">
            </Component>
            <Component class="org.jdesktop.swingx.JXDatePicker" name="jXDatePicker2">
            </Component>
          </SubComponents>
        </Container>
        <Container class="javax.swing.JPanel" name="jPanel2">
          <Properties>
            <Property name="font" type="java.awt.Font" editor="org.netbeans.beaninfo.editors.FontEditor">
              <Font name="Tahoma" size="14" style="0"/>
            </Property>
          </Properties>
          <Constraints>
            <Constraint layoutClass="org.netbeans.modules.form.compat2.layouts.DesignCardLayout" value="org.netbeans.modules.form.compat2.layouts.DesignCardLayout$CardConstraintsDescription">
              <CardConstraints cardName="result page"/>
            </Constraint>
          </Constraints>

          <Layout>
            <DimensionLayout dim="0">
              <Group type="103" groupAlignment="0" attributes="0">
                  <Component id="jScrollPane1" max="32767" attributes="0"/>
                  <Group type="102" attributes="0">
                      <EmptySpace min="-2" pref="41" max="-2" attributes="0"/>
                      <Component id="jTextField2" min="-2" pref="664" max="-2" attributes="0"/>
                      <EmptySpace min="-2" pref="27" max="-2" attributes="0"/>
                      <Component id="jButton2" min="-2" pref="89" max="-2" attributes="0"/>
                      <EmptySpace min="0" pref="0" max="32767" attributes="0"/>
                  </Group>
                  <Group type="102" alignment="1" attributes="0">
                      <EmptySpace max="-2" attributes="0"/>
                      <Component id="jLabel11" max="32767" attributes="0"/>
                      <EmptySpace max="-2" attributes="0"/>
                      <Component id="jLabel12" min="-2" pref="204" max="-2" attributes="0"/>
                  </Group>
                  <Group type="102" alignment="0" attributes="0">
                      <EmptySpace max="-2" attributes="0"/>
                      <Component id="jScrollPane2" max="32767" attributes="0"/>
                  </Group>
                  <Group type="102" alignment="1" attributes="0">
                      <EmptySpace max="32767" attributes="0"/>
                      <Component id="jButton3" min="-2" pref="93" max="-2" attributes="0"/>
                      <EmptySpace max="-2" attributes="0"/>
                  </Group>
              </Group>
            </DimensionLayout>
            <DimensionLayout dim="1">
              <Group type="103" groupAlignment="0" attributes="0">
                  <Group type="102" alignment="0" attributes="0">
                      <Component id="jScrollPane1" min="-2" pref="26" max="-2" attributes="0"/>
                      <EmptySpace max="-2" attributes="0"/>
                      <Group type="103" groupAlignment="3" attributes="0">
                          <Component id="jButton2" alignment="3" min="-2" pref="26" max="-2" attributes="0"/>
                          <Component id="jTextField2" alignment="3" min="-2" pref="26" max="-2" attributes="0"/>
                      </Group>
                      <EmptySpace max="-2" attributes="0"/>
                      <Group type="103" groupAlignment="0" max="-2" attributes="0">
                          <Component id="jLabel11" pref="34" max="32767" attributes="0"/>
                          <Component id="jLabel12" max="32767" attributes="0"/>
                      </Group>
                      <EmptySpace min="-2" pref="31" max="-2" attributes="0"/>
                      <Component id="jScrollPane2" min="-2" pref="144" max="-2" attributes="0"/>
                      <EmptySpace type="separate" max="-2" attributes="0"/>
                      <Component id="jButton3" min="-2" max="-2" attributes="0"/>
                      <EmptySpace min="0" pref="20" max="32767" attributes="0"/>
                  </Group>
              </Group>
            </DimensionLayout>
          </Layout>
          <SubComponents>
            <Container class="javax.swing.JScrollPane" name="jScrollPane1">
              <AuxValues>
                <AuxValue name="autoScrollPane" type="java.lang.Boolean" value="true"/>
              </AuxValues>

              <Layout class="org.netbeans.modules.form.compat2.layouts.support.JScrollPaneSupportLayout"/>
              <SubComponents>
                <Component class="javax.swing.JTable" name="jTable1">
                  <Properties>
                    <Property name="model" type="javax.swing.table.TableModel" editor="org.netbeans.modules.form.editors2.TableModelEditor">
                      <Table columnCount="7" rowCount="0">
                        <Column editable="false" title="Sort by:" type="java.lang.String"/>
                        <Column editable="false" title="Airline" type="java.lang.String"/>
                        <Column editable="false" title="Airport" type="java.lang.String"/>
                        <Column editable="false" title="Departure time" type="java.lang.String"/>
                        <Column editable="false" title="Arrival time" type="java.lang.String"/>
                        <Column editable="false" title="Flight duration" type="java.lang.String"/>
                        <Column editable="false" title="Number of stops" type="java.lang.String"/>
                      </Table>
                    </Property>
                    <Property name="columnModel" type="javax.swing.table.TableColumnModel" editor="org.netbeans.modules.form.editors2.TableColumnModelEditor">
                      <TableColumnModel selectionModel="0">
                        <Column maxWidth="-1" minWidth="-1" prefWidth="-1" resizable="true">
                          <Title/>
                          <Editor/>
                          <Renderer/>
                        </Column>
                        <Column maxWidth="-1" minWidth="-1" prefWidth="-1" resizable="true">
                          <Title/>
                          <Editor/>
                          <Renderer/>
                        </Column>
                        <Column maxWidth="-1" minWidth="-1" prefWidth="-1" resizable="true">
                          <Title/>
                          <Editor/>
                          <Renderer/>
                        </Column>
                        <Column maxWidth="-1" minWidth="-1" prefWidth="-1" resizable="true">
                          <Title/>
                          <Editor/>
                          <Renderer/>
                        </Column>
                        <Column maxWidth="-1" minWidth="-1" prefWidth="-1" resizable="true">
                          <Title/>
                          <Editor/>
                          <Renderer/>
                        </Column>
                        <Column maxWidth="-1" minWidth="-1" prefWidth="-1" resizable="true">
                          <Title/>
                          <Editor/>
                          <Renderer/>
                        </Column>
                        <Column maxWidth="-1" minWidth="-1" prefWidth="-1" resizable="true">
                          <Title/>
                          <Editor/>
                          <Renderer/>
                        </Column>
                      </TableColumnModel>
                    </Property>
                    <Property name="rowHeight" type="int" value="50"/>
                    <Property name="rowMargin" type="int" value="3"/>
                    <Property name="tableHeader" type="javax.swing.table.JTableHeader" editor="org.netbeans.modules.form.editors2.JTableHeaderEditor">
                      <TableHeader reorderingAllowed="true" resizingAllowed="true"/>
                    </Property>
                  </Properties>
                </Component>
              </SubComponents>
            </Container>
            <Component class="javax.swing.JButton" name="jButton2">
              <Properties>
                <Property name="text" type="java.lang.String" value="Next day"/>
              </Properties>
            </Component>
            <Component class="javax.swing.JTextField" name="jTextField2">
              <Properties>
                <Property name="editable" type="boolean" value="false"/>
                <Property name="font" type="java.awt.Font" editor="org.netbeans.beaninfo.editors.FontEditor">
                  <Font name="Tahoma" size="12" style="0"/>
                </Property>
                <Property name="horizontalAlignment" type="int" value="0"/>
                <Property name="text" type="java.lang.String" value="Selected date : Sunday, November 30 2015"/>
              </Properties>
              <Events>
                <EventHandler event="actionPerformed" listener="java.awt.event.ActionListener" parameters="java.awt.event.ActionEvent" handler="jTextField2ActionPerformed"/>
              </Events>
            </Component>
            <Component class="javax.swing.JLabel" name="jLabel11">
              <Properties>
                <Property name="font" type="java.awt.Font" editor="org.netbeans.beaninfo.editors.FontEditor">
                  <Font name="Tahoma" size="14" style="0"/>
                </Property>
                <Property name="text" type="java.lang.String" value="Lufthansa"/>
                <Property name="border" type="javax.swing.border.Border" editor="org.netbeans.modules.form.editors2.BorderEditor">
                  <Border info="org.netbeans.modules.form.compat2.border.EtchedBorderInfo">
                    <EtchetBorder/>
                  </Border>
                </Property>
              </Properties>
            </Component>
            <Component class="javax.swing.JLabel" name="jLabel12">
              <Properties>
                <Property name="font" type="java.awt.Font" editor="org.netbeans.beaninfo.editors.FontEditor">
                  <Font name="Tahoma" size="14" style="0"/>
                </Property>
                <Property name="text" type="java.lang.String" value="Flight numbers: LH9719/LH4234"/>
                <Property name="border" type="javax.swing.border.Border" editor="org.netbeans.modules.form.editors2.BorderEditor">
                  <Border info="org.netbeans.modules.form.compat2.border.EtchedBorderInfo">
                    <EtchetBorder/>
                  </Border>
                </Property>
              </Properties>
            </Component>
            <Container class="javax.swing.JScrollPane" name="jScrollPane2">
              <AuxValues>
                <AuxValue name="autoScrollPane" type="java.lang.Boolean" value="true"/>
              </AuxValues>

              <Layout class="org.netbeans.modules.form.compat2.layouts.support.JScrollPaneSupportLayout"/>
              <SubComponents>
                <Component class="javax.swing.JTable" name="jTable2">
                  <Properties>
                    <Property name="border" type="javax.swing.border.Border" editor="org.netbeans.modules.form.editors2.BorderEditor">
                      <Border info="org.netbeans.modules.form.compat2.border.BevelBorderInfo">
                        <BevelBorder>
                          <Color PropertyName="shadowInner" blue="ec" green="ee" red="ee" type="rgb"/>
                        </BevelBorder>
                      </Border>
                    </Property>
                    <Property name="model" type="javax.swing.table.TableModel" editor="org.netbeans.modules.form.editors2.TableModelEditor">
                      <Table columnCount="9" rowCount="1">
                        <Column editable="true" title="null" type="java.lang.Object"/>
                        <Column editable="true" title="Title 2" type="java.lang.Object">
                          <Data value="Bangkok(BKK) Bangkok(BKK) Frankfurt(FRA) "/>
                        </Column>
                        <Column editable="true" title="Title 3" type="java.lang.Object">
                          <Data value="-Paris(CDG) -Frankfurt(FRA) -Paris(CDG)"/>
                        </Column>
                        <Column editable="true" title="Title 4" type="java.lang.Object"/>
                        <Column editable="true" title="Title 5" type="java.lang.Object">
                          <Data value="Sun 30 Nov"/>
                        </Column>
                        <Column editable="true" title="Title 6" type="java.lang.Object">
                          <Data value="12:55 12:55 20:20"/>
                        </Column>
                        <Column editable="true" title="Title 7" type="java.lang.Object">
                          <Data value="-21:30 18:45 -21:30 "/>
                        </Column>
                        <Column editable="true" title="Title 8" type="java.lang.Object">
                          <Data value="14h 35 11h 50 1h 10"/>
                        </Column>
                        <Column editable="true" title="Title 9" type="java.lang.Object">
                          <Data value="1 Stop"/>
                        </Column>
                      </Table>
                    </Property>
                    <Property name="columnModel" type="javax.swing.table.TableColumnModel" editor="org.netbeans.modules.form.editors2.TableColumnModelEditor">
                      <TableColumnModel selectionModel="0">
                        <Column maxWidth="-1" minWidth="-1" prefWidth="-1" resizable="true">
                          <Title/>
                          <Editor/>
                          <Renderer/>
                        </Column>
                        <Column maxWidth="-1" minWidth="-1" prefWidth="-1" resizable="true">
                          <Title/>
                          <Editor/>
                          <Renderer/>
                        </Column>
                        <Column maxWidth="-1" minWidth="-1" prefWidth="-1" resizable="true">
                          <Title/>
                          <Editor/>
                          <Renderer/>
                        </Column>
                        <Column maxWidth="-1" minWidth="-1" prefWidth="-1" resizable="true">
                          <Title/>
                          <Editor/>
                          <Renderer/>
                        </Column>
                        <Column maxWidth="-1" minWidth="-1" prefWidth="-1" resizable="true">
                          <Title/>
                          <Editor/>
                          <Renderer/>
                        </Column>
                        <Column maxWidth="-1" minWidth="-1" prefWidth="-1" resizable="true">
                          <Title/>
                          <Editor/>
                          <Renderer/>
                        </Column>
                        <Column maxWidth="-1" minWidth="-1" prefWidth="-1" resizable="true">
                          <Title/>
                          <Editor/>
                          <Renderer/>
                        </Column>
                        <Column maxWidth="-1" minWidth="-1" prefWidth="-1" resizable="true">
                          <Title/>
                          <Editor/>
                          <Renderer/>
                        </Column>
                      </TableColumnModel>
                    </Property>
                    <Property name="rowHeight" type="int" value="120"/>
                    <Property name="rowMargin" type="int" value="0"/>
                    <Property name="tableHeader" type="javax.swing.table.JTableHeader" editor="org.netbeans.modules.form.RADConnectionPropertyEditor">
                      <Connection code="null" type="code"/>
                    </Property>
                  </Properties>
                </Component>
              </SubComponents>
            </Container>
            <Component class="javax.swing.JButton" name="jButton3">
              <Properties>
                <Property name="text" type="java.lang.String" value="Best fares"/>
              </Properties>
            </Component>
          </SubComponents>
        </Container>
      </SubComponents>
    </Container>
  </SubComponents>
</Form>